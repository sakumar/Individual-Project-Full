package camerashots.droid101;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }


    public void sendMessage(View view) {

        switch (view.getId()) {

                     case R.id.imagebackButton: {
                //do something
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            }
        }

    }
}
