package camerashots.droid101;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void sendMessage(View view) {

        switch (view.getId()) {
            case R.id.playbutton: {
                //do something
                Intent intent = new Intent(this, playActivity.class);
                startActivity(intent);
                break;
            }

            case R.id.aboutbutton: {
                //do something
                Intent intent = new Intent(this, About.class);
                startActivity(intent);
                break;
            }

            case R.id.settingsbutton: {
                //do something
                Intent intent = new Intent(this, Settings.class);
                startActivity(intent);
                break;
            }
        }

    }

}