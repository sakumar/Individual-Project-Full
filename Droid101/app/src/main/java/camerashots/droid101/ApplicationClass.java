package camerashots.droid101;

import android.app.Application;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class ApplicationClass extends Application {

    Boolean sound_state;

    public RadioGroup settings_sound;
   public RadioButton sound_on, sound_off;

        SoundPool globalsoundPool;
        SoundPool.Builder globalsoundPoolBuilder;

        AudioAttributes globalattributes;
        AudioAttributes.Builder globalattributesBuilder;

    public Boolean getSound_state() {
        return sound_state;
    }

    public void setSound_state(Boolean sound_state) {
        this.sound_state = sound_state;
    }



    @Override
    public void onCreate( ) {
        super.onCreate();

            setSound_state(true);
            gcreateSoundPool();

        }

    public void gcreateSoundPool(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            globalattributesBuilder = new AudioAttributes.Builder();
            globalattributesBuilder.setUsage(AudioAttributes.USAGE_GAME);
            globalattributesBuilder.setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION);
            globalattributes = globalattributesBuilder.build();

            globalsoundPoolBuilder= new SoundPool.Builder();
            globalsoundPoolBuilder.setAudioAttributes(globalattributes);
            globalsoundPool = globalsoundPoolBuilder.build();

        }else{
            // This SoundPool is deprecated but don't worry
            globalsoundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        }

    }
}