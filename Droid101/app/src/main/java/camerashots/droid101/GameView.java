package camerashots.droid101;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by camer on 09/12/2016.
 */

public class GameView extends SurfaceView implements Runnable {

    Thread thread = null;
    boolean CanDraw = false;

    Bitmap background;
    Canvas canvas;
    SurfaceHolder surfaceholder;

    public GameView(Context context) {
        super(context);
         surfaceholder = getHolder();
        background = BitmapFactory.decodeResource(getResources(),R.drawable.bg);

        //setBackgroundResource(R.drawable.bg);
    }


    @Override
    public void run() {

        while(CanDraw){

            if(!surfaceholder.getSurface().isValid()){
                continue;
            }

            canvas = surfaceholder.lockCanvas();
            canvas.drawBitmap(background,0,0,null);
            surfaceholder.unlockCanvasAndPost(canvas);

        }

    }

    public void resume() {
        CanDraw = true;
        thread = new Thread(this);
        thread.start();

    }

    public void pause() {

        CanDraw = false;

        while(true){
        try {
            thread.join();
            break;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    } thread = null;

    }
}
