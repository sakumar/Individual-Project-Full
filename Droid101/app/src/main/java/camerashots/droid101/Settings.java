package camerashots.droid101;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class Settings extends AppCompatActivity {


    protected ApplicationClass app;

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

       app=(ApplicationClass)getApplication();

       app.settings_sound=(RadioGroup)findViewById(R.id.settings_sound);
       app.sound_on=(RadioButton)findViewById(R.id.sound_on);
       app.sound_off=(RadioButton)findViewById(R.id.sound_off);



       if(app.getSound_state()){
           app.sound_on.setChecked(true);
       }else{
          app.sound_off.setChecked(true);
       }

    }

    public void sound_on_click(View view){
        Toast.makeText(getApplicationContext(), "Sfx on", Toast.LENGTH_SHORT).show();
        app.setSound_state(true);
    }

    public void sound_off_click(View view){
        Toast.makeText(getApplicationContext(), "Sfx off", Toast.LENGTH_SHORT).show();
        app.setSound_state(false);
    }

    public void sendMessage(View view) {

        switch (view.getId()) {

            case R.id.imagebackButtonsettings: {
                //do something
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            }
        }

    }

}

