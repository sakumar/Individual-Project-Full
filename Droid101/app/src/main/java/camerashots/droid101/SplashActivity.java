package camerashots.droid101;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

public class SplashActivity extends AppCompatActivity {

    public RelativeLayout relativeLayout;
    public static int splashimage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    relativeLayout =(RelativeLayout) findViewById(R.id.activity_splash);

        if (splashimage==0)
        {
            relativeLayout.setBackgroundResource(R.drawable.splash);
        }
        else{
            relativeLayout.setBackgroundResource(splashimage);
        }

        Thread displaySplash = new Thread() {
            public void run (){
                try {
                    sleep(5 * 1000);
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(intent);

                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    //kill app
                    finish();
                }
            }

        };
        displaySplash.start();
    }
}
