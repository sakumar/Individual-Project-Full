package camerashots.droid101;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class playActivity extends AppCompatActivity {
    // gameView will be the view of the game
    // It will also hold the logic of the game
    // and respond to screen touches as well

    protected ApplicationClass app;

    PlayActivityview playActivityview;
    int globalbeep1ID;
    int globalbeep2ID;
    int globalbeep3ID;
    int globalloseLifeID;
    int globalexplodeID;

    //ImageView myImageView;
    Bitmap ballpic;
    int ballpic_x, ballpic_y;
    int x_dir, y_dir;

    Bitmap goalpic;
    int goalpic_x, goalpic_y;
    int goalxdir, goalydir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setContentView(R.layout.activity_play);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        // Initialize gameView and set it as the view
        playActivityview = new PlayActivityview(this);
        setContentView(playActivityview);

        app=(ApplicationClass)getApplication();
        gloadSounds();
        playsound1();
        playsound2();
        playsound3();
        playsound4();
        playsound5();
        //  app.gcreateSoundPool();


    }

    public void gloadSounds(){
        globalbeep1ID = app.globalsoundPool.load(this,R.raw.beep1,1);
        globalbeep2ID = app.globalsoundPool.load(this,R.raw.beep2,2);
        globalbeep3ID = app.globalsoundPool.load(this,R.raw.beep3,3);
        globalloseLifeID = app.globalsoundPool.load(this,R.raw.loselife,4);
        globalexplodeID = app.globalsoundPool.load(this,R.raw.explode,5);


    }

    public void playsound1(){
        if (app.sound_state == true){

            app.globalsoundPool.play(globalexplodeID, 1, 1, 0, 0, 1);

    }}

    public void playsound2(){
        if (app.sound_state == true){
        app.globalsoundPool.play(globalbeep1ID, 1, 1, 0, 0, 1);

    }}

    public void playsound3(){
        if (app.sound_state == true){
           app.globalsoundPool.play(globalbeep3ID, 1, 1, 0, 0, 1);

    }}

    public void playsound4(){
        if (app.sound_state == true){

        app.globalsoundPool.play(globalbeep2ID, 1, 1, 0, 0, 1);

    }}
    public void playsound5() {
        if (app.sound_state == true) {

            app.globalsoundPool.play(globalloseLifeID, 1, 1, 0, 0, 1);

        }
    }


    // Here is our implementation of PlayActivityview
    // It is an inner class.
    // Note how the final closing curly brace }
    // is inside the PlayActivity class

    // Notice we implement runnable so we have
    // A thread and can override the run method.
    class PlayActivityview extends SurfaceView implements Runnable {

        //int volume1 = 1;

        // This is our thread
        Thread gameThread = null;

        // This is new. We need a SurfaceHolder
        // When we use Paint and Canvas in a thread
        // We will see it in action in the draw method soon.
        SurfaceHolder ourHolder;

        // A boolean which we will set and unset
        // when the game is running- or not.
        volatile boolean playing;

        // Game is paused at the start
        boolean paused = true;

        // A Canvas and a Paint object
        Canvas canvas;
        Paint paint;

        // This variable tracks the game frame rate
        long fps;

        // This is used to help calculate the fps
        private long timeThisFrame;

        // The size of the screen in pixels
        int screenX;
        int screenY;

        // The players paddle
        //  Paddle paddle;
        Paddle2 paddle2;

        Ballpicout ballpicout;

        // A ball
        Ball ball;


        Goal2 goal2;

        // The score
        int score = 0;

        // Lives
        int lives = 3;

        // When the we initialize (call new()) on gameView
        // This special constructor method runs
        public PlayActivityview(Context context) {
            // The next line of code asks the
            // SurfaceView class to set up our object.
            // How kind.
            super(context);
            //this.context = context;
            // Initialize ourHolder and paint objects
            ourHolder = getHolder();
            paint = new Paint();

            // Get a Display object to access screen details
            Display display = getWindowManager().getDefaultDisplay();
            // Load the resolution into a Point object
            Point size = new Point();
            display.getSize(size);

            screenX = size.x;
            screenY = size.y;



            // Make a new goal2
            goal2 = new Goal2(context, screenX, screenY);

            //new paddle
            paddle2 = new Paddle2(context, screenX, screenY);

            ballpicout = new Ballpicout (screenX, screenY);

            // Create a ball
            ball = new Ball(screenX, screenY);

            ballpic_x = 320;
            ballpic_y = 470;

            x_dir = 3;
            y_dir = 3;

            goalpic_x = (screenX/2)-100;
            goalpic_y = 0;
            goalxdir = 3;
            goalydir = 3;

            createGoalAndRestart();

        }



        public void createGoalAndRestart() {

            // Put the ball back to the start
            ball.reset(screenX, screenY);


            // if game over reset scores and lives
            if (lives == 0) {
                score = 0;
                lives = 3;
            }
        }


        @Override
        public void run() {
            while (playing) {

                // Capture the current time in milliseconds in startFrameTime
                long startFrameTime = System.currentTimeMillis();

                // Update the frame
                if (!paused) {
                    update();
                }

                // Draw the frame
                draw();

                // Calculate the fps this frame
                // We can then use the result to
                // time animations and more.
                timeThisFrame = System.currentTimeMillis() - startFrameTime;
                if (timeThisFrame >= 1) {
                    fps = 1000 / timeThisFrame;
                }

            }

        }

        // Everything that needs to be updated goes in here
        // Movement, collision detection etc.
        public void update() {

            // Move the paddle if required

            ball.update(fps);
            ballpicout.update(fps);
            paddle2.update(fps);

            goal2.update(fps);
            ballpic_x = ballpic_x + x_dir;
            ballpic_y = ballpic_y + y_dir;

            if (ballpic_x > screenX - 35){
                x_dir = -3;

            }

            if (ballpic_x < 0){
                x_dir = 3;

            }

            if (ballpic_y > screenY - 35 ){
                y_dir = -3;

            }

            if (ballpic_y < 0){
                y_dir = 3;

            }
/////


            // Check for ball colliding with Goal2
            if (RectF.intersects(goal2.getRect(), ballpicout.getRect()) ) {
                ballpicout.setRandomXVelocity();
                ballpicout.reverseYVelocity();
                ballpicout.clearObstacleY(goal2.getRect().bottom + 12);
                ballpicout.clearObstacleY(goal2.getRect().right + 12);
                ballpicout.clearObstacleY(goal2.getRect().left + 12);
                score = score + 10;
                playsound1();

            }


            // Check for ball colliding with paddle
            if (RectF.intersects(paddle2.getRect(), ballpicout.getRect()) ) {
                ballpicout.setRandomXVelocity();
                ballpicout.reverseYVelocity();
                ballpicout.clearObstacleY(paddle2.getRect().top - 2);

                playsound2();

            }

            // Bounce the ball back when it hits the bottom of screen
            if (ballpicout.getRect().bottom > screenY ) {
                ballpicout.reverseYVelocity();
                ballpicout.clearObstacleY(screenY - 2);
                // Lose a life
                lives--;
                playsound3();
                }



            // Bounce the ball back when it hits the top of screen
            if (ballpicout.getRect().top < 0 ) {
                ballpicout.reverseYVelocity();
                ballpicout.clearObstacleY(22);

                playsound4();           }

            // If the ball hits left wall bounce
            if (ballpicout.getRect().left < 0 ) {
                ballpicout.reverseXVelocity();
                ballpicout.clearObstacleX(2);

                playsound5();          }

            // If the ball hits right wall bounce
            if (ballpicout.getRect().right > screenX - 10 ) {
                ballpicout.reverseXVelocity();
                ballpicout.clearObstacleX(screenX - 42);


                playsound5();           }

            // Pause if cleared screen
          //  if (score == 10 * 10) {
         //       paused = true;
         //       createGoalAndRestart();
         //   }




//////


            // Check for ball colliding with Goal2
            if (RectF.intersects(goal2.getRect(), ball.getRect()) ) {
                ball.setRandomXVelocity();
                ball.reverseYVelocity();
                ball.clearObstacleY(goal2.getRect().bottom + 12);
                ball.clearObstacleY(goal2.getRect().right + 12);
                ball.clearObstacleY(goal2.getRect().left + 12);
                score = score + 10;
                playsound1();

                }


            // Check for ball colliding with paddle
            if (RectF.intersects(paddle2.getRect(), ball.getRect()) ) {
                ball.setRandomXVelocity();
                ball.reverseYVelocity();
                ball.clearObstacleY(paddle2.getRect().top - 2);

                playsound2();

            }

            // Bounce the ball back when it hits the bottom of screen
            if (ball.getRect().bottom > screenY ) {
                ball.reverseYVelocity();
                ball.clearObstacleY(screenY - 2);
                // Lose a life
                lives--;
                playsound3();
                }

            if (lives <= 0) {
                paused = true;
                               //createGoalAndRestart();
            }



            // Bounce the ball back when it hits the top of screen
            if (ball.getRect().top < 0 ) {
                ball.reverseYVelocity();
                ball.clearObstacleY(22);

                playsound4();           }

            // If the ball hits left wall bounce
            if (ball.getRect().left < 0 ) {
                ball.reverseXVelocity();
                ball.clearObstacleX(2);

                playsound5();          }

            // If the ball hits right wall bounce
            if (ball.getRect().right > screenX - 10 ) {
                ball.reverseXVelocity();
                ball.clearObstacleX(screenX - 42);


                playsound5();           }

            // Pause if cleared screen
            if (score == 10 * 10) {

                paused = true;
                createGoalAndRestart();


            }

        }

        // Draw the newly updated scene
        public void draw() {

            // Make sure our drawing surface is valid or we crash
            if (ourHolder.getSurface().isValid()) {
                // Lock the canvas ready to draw
                canvas = ourHolder.lockCanvas();

                // Draw the background color
                canvas.drawColor(Color.argb(255, 26, 128, 182));

                paint.setColor(Color.argb(255,0,0, 8));

                // Draw the ball
                canvas.drawRect(ball.getRect(), paint);

                canvas.drawRect(ballpicout.getRect(), paint);

                // Now draw the player GOAL2
                canvas.drawBitmap(goal2.getBitmap(), goal2.getX(), screenY - (screenY+10), paint);

                // Now draw the player PADDLE2
                canvas.drawBitmap(paddle2.getBitmap(), paddle2.getX(), screenY - 70, paint);

               // canvas.drawBitmap(ballpicout.getBitmap(), screenX - 100 , screenY - 260, paint);


                //ballpic= BitmapFactory.decodeResource(getResources(),R.drawable.ball);
               // canvas.drawBitmap(ballpic,ballpic_x,ballpic_y,null);

               //goalpic= BitmapFactory.decodeResource(getResources(),R.drawable.goalpic);
              //  canvas.drawBitmap(goalpic,goalpic_x,goalpic_y,null);
                // Choose the brush color for drawing
               // paint.setColor(Color.argb(255, 255, 255, 255));

                // Choose the brush color for drawing
                paint.setColor(Color.argb(255, 255, 255, 255));

                // Draw the score
                paint.setTextSize(50);
                canvas.drawText("$: " + score , 10, 50, paint);
                canvas.drawText("❤: " + lives, 490, 50, paint);

                // Has the player cleared the screen?
                if (score == 10 * 10) {
                    paint.setTextSize(60);
                    canvas.drawText("YOU HAVE WON!", 10,550, paint);


                }

                paint.setColor(Color.argb(255, 255, 255, 255));
                // Has the player lost?
                if (lives <= 0) {
                    paint.setTextSize(60);
                    canvas.drawText("YOU HAVE LOST!", 10,550, paint);

                }

                // Draw everything to the screen
                ourHolder.unlockCanvasAndPost(canvas);
            }

        }

        // If SimpleGameEngine Activity is paused/stopped
        // shutdown our thread.
        public void pause() {
            playing = false;
            try {
                gameThread.join();
            } catch (InterruptedException e) {
                Log.e("Error:", "joining thread");
            }

        }

        // If SimpleGameEngine Activity is started theb
        // start our thread.
        public void resume() {
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        // The SurfaceView class implements onTouchListener
        // So we can override this method and detect screen touches.
        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {

            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {


                // Player has touched the screen
                case MotionEvent.ACTION_DOWN:

                    paused = false;


                    if (motionEvent.getX() > screenX / 2) {
                        paddle2.setMovementState(paddle2.RIGHT);
                    } else if (motionEvent.getX() < screenX / 2) {
                        paddle2.setMovementState(paddle2.LEFT);
                    }

                    break;


                // Player has removed finger from screen
                case MotionEvent.ACTION_UP:

                    paddle2.setMovementState(paddle2.STOPPED);
                    break;
            }


            return true;
        }

    }
    // This is the end of our PlayActivityview inner class

    // This method executes when the player starts the game
    @Override
    protected void onResume() {
        super.onResume();
        app.gcreateSoundPool();
        gloadSounds();


                // Tell the gameView resume method to execute
        playActivityview.resume();
    }

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();

        app.globalsoundPool.release();
        // Tell the gameView pause method to execute
        playActivityview.pause();
    }


// This is the end of the PlayActivity class

}

